//
//  RSTableViewController.m
//  NewsPoint
//
//  Created by Rohit Singh on 10/09/2014.
//  Copyright (c) 2014 ROHIT. All rights reserved.
//

#import "RSTableViewController.h"
#import "RSElement.h"
#import "RSViewController.h"

@interface RSTableViewController ()

@end

@implementation RSTableViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.request = [NSURLRequest requestWithURL:[NSURL URLWithString:@"http://livepost.in/json/top"]];
    
    // Create url connection and fire request
    NSURLConnection *conn = [[NSURLConnection alloc] initWithRequest:self.request delegate:self];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{

    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(self.storeData)
        return [self.storeData count];
    else
    return 0;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" ];
   
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"] ;
    }
    
    if(self.storeData){
    
        RSElement *element=[self.storeData objectAtIndex:indexPath.row];
        cell.textLabel.text=element.title;
        cell.detailTextLabel.text=element.description;
    }
    
    return cell;
}
-(void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response{
    self.responseData=[[NSMutableData alloc]init];
    
}
-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data{
    [self.responseData appendData:data];
}
-(void)connectionDidFinishLoading:(NSURLConnection *)connection{
    NSString *s= [[NSString alloc] initWithData:self.responseData encoding:NSASCIIStringEncoding];
    NSLog(@"%@",s);
    NSData *datareceived=[NSData dataWithData:self.responseData];
    //parse out the json data
    NSError* error;
    NSDictionary* json = [NSJSONSerialization
                          JSONObjectWithData:datareceived //1
                          
                          options:kNilOptions
                          error:&error];
    
    NSArray* list = [json objectForKey:@"NewsList"];
    
    self.storeData=[[NSMutableArray alloc]init];
    for(NSDictionary *d in list){
        RSElement *elemnt=[[RSElement alloc]init];
        elemnt.description=d[@"Description"];
        elemnt.title=d[@"Title"];
        elemnt.url=d[@"ShortUrl"];
        [self.storeData addObject:elemnt];
        
    }
    [self.tableView reloadData];
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    
    if([sender isKindOfClass:[UITableViewCell class]])
    if ([[segue identifier] isEqualToString:@"sendurl"])
    {
        RSViewController *dvc = segue.destinationViewController;
        UITableViewCell *cell = (UITableViewCell*)sender;
        
        NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
        NSLog(@"%i",indexPath.row);
        RSElement *eelement=[self.storeData objectAtIndex:indexPath.row];
        NSString *title=eelement.title;
        dvc.element=eelement;
        NSLog(@"%@",title);
        
    }
    
}


@end
