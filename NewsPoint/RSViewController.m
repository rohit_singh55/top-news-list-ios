//
//  RSViewController.m
//  NewsPoint
//
//  Created by Rohit Singh on 10/09/2014.
//  Copyright (c) 2014 ROHIT. All rights reserved.
//

#import "RSViewController.h"
#import "RSElement.h"

@interface RSViewController ()

@property (strong, nonatomic) IBOutlet UIWebView *webview;



@end
@implementation RSViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    // Create the request.
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:self.element.url]];
    [self.webview loadRequest:request];
    
    
   
    
    
}

-(void)webViewDidStartLoad:(UIWebView *)webView{
}

-(void)webViewDidFinishLoad:(UIWebView *)webView{

   
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    
}

@end
