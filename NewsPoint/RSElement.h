//
//  RSElement.h
//  NewsPoint
//
//  Created by Rohit Singh on 10/09/2014.
//  Copyright (c) 2014 ROHIT. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RSElement : NSObject
@property(nonatomic,strong) NSString *description;
@property(nonatomic,strong) NSString *url;
@property(nonatomic,strong) NSString *title;


@end
