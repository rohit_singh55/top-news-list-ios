//
//  RSTableViewController.h
//  NewsPoint
//
//  Created by Rohit Singh on 10/09/2014.
//  Copyright (c) 2014 ROHIT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RSViewController.h"

@interface RSTableViewController : UITableViewController<NSURLConnectionDataDelegate>
@property(nonatomic,strong) NSMutableData *responseData;
@property(nonatomic,strong) NSURLRequest *request;
@property(nonatomic,strong) NSMutableArray *storeData;

@end
